#!/usr/bin/env node

const colors = require('colors');
const moment = require('moment');
const args = require('minimist')(process.argv.slice(2));
const AWS = require('@aws-sdk/client-ecs');
const ECS = new AWS.ECS({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_DEFAULT_REGION || 'us-east-1',
});

const intervalBetweenRequests = 5; // In seconds

const option = args._.length > 0 ? args._[0] : null;
const params = {
  clusterName: args.cluster || args.c,
  serviceName: args.service || args.s,
  taskDefinition: args['task-definition'],
  timeout: args.timeout || 1200,
}

if (!option) {
  console.error('No option was specified, please use one of the following:\n\tservice-stable => Wait till an specified service becomes stable'.red);
  process.exit(1)
}

if (!['service-stable'].includes(option)) {
  console.error('Specified option is invalid, use [service-stable]'.red);
  process.exit(1);
}

if (!params.clusterName) {
  console.error('Cluster name must be specified'.red);
  process.exit(1);
}

if (!params.serviceName) {
  console.error('Service name must be specified'.red);
  process.exit(1);
}

if (!params.taskDefinition) {
  console.error('Task definition must be specified'.red);
  process.exit(1);
}

function describeCluster(clusterName) {
  return new Promise((resolve, reject) => {
    ECS.describeClusters({ clusters: [clusterName] }, function(err, data) {
      if (err) return reject(err);
      if (data.failures.length > 0 || data.clusters.length < 1) return reject(data.failures[0]);
      return resolve(data.clusters[0]);
    });
  });
}

function describeService(clusterName, serviceName) {
  return new Promise((resolve, reject) => {
    ECS.describeServices({ cluster: clusterName, services: [serviceName] }, function(err, data) {
      if (err) return reject(err);
      if (data.failures.length > 0 || data.services.length < 1) return reject(data.failures[0]);
      return resolve(data.services[0]);
    });
  });
}

function describeTaskDefinition(taskDefinition) {
  return new Promise((resolve, reject) => {
    ECS.describeTaskDefinition({ taskDefinition }, function(err, data) {
      if (err) return reject(err);
      return resolve(data.taskDefinition);
    });
  });
}

function wait (seconds) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), seconds * 1000);
  });
}

(async function() {
  console.log(`Preparing the system ⏳`.white);
  await wait(5);

  let cluster = null;
  try {
    cluster = await describeCluster(params.clusterName);
  } catch (error) {
    if (error.reason === 'MISSING') {
      console.error('Specified cluster does not exist ❌'.red);
      process.exit(1);
    }
  }

  let service = null;
  try {
    service = await describeService(params.clusterName, params.serviceName);
  } catch (error) {
    if (error.reason === 'MISSING') {
      console.error('Specified service does not exist ❌'.red);
      process.exit(1);
    }
  }

  let taskDefinition = null;
  try {
    taskDefinition = await describeTaskDefinition(params.taskDefinition);
  } catch (error) {
    console.error('Specified task definition does not exist ❌'.red);
    process.exit(1);
  }

  let deployment = service.deployments.find((deployment) => deployment.taskDefinition === taskDefinition.taskDefinitionArn);
  if (!deployment) {
    console.error('Deployment has not being found ❌'.red);
    process.exit(1);
  }

  let totalSeconds = 0;
  
  for(let i=0; ;i++) {
    const start = moment();
    service = await describeService(params.clusterName, params.serviceName);
    deployment = service.deployments.find((deployment) => deployment.taskDefinition === taskDefinition.taskDefinitionArn);
    const { desiredCount, pendingCount, runningCount, rolloutState } = deployment;

    if (rolloutState == 'COMPLETED') {
      break;
    } else if (!['IN_PROGRESS', 'COMPLETED'].includes(rolloutState)) {
      console.error(`Deployment failed ❌`.red);
      console.log(`Last rollout state: ${rolloutState}`.red);
      process.exit(1);
    }
  
    console.log(`Deployment task count: Expected[${desiredCount}] Running[${runningCount}] Pending[${pendingCount}] ElapsedTime[${totalSeconds.toFixed(3)}s] ${i % 2 === 0 ? '⏳' : '⌛'} please take a ☕`.cyan);
    console.log(`Rollout state: ${rolloutState}`.cyan);

    const end = moment();
    const duration = moment.duration(end.diff(start)).asSeconds();

    totalSeconds += duration;

    if (totalSeconds > params.timeout) {
      console.error(`Timeout, this service is taking too long to stabilize ElapsedTime[${totalSeconds.toFixed(3)}s] ❌`.red);
      console.log(`Last rollout state: ${rolloutState}`.red);
      process.exit(1);
    }

    await wait(intervalBetweenRequests);
  };

  service = await describeService(params.clusterName, params.serviceName);
  deployment = service.deployments.find((deployment) => deployment.taskDefinition === taskDefinition.taskDefinitionArn);
  const { desiredCount, pendingCount, runningCount, rolloutState } = deployment;
  console.log(`Deployment tasks deployed: Expected[${desiredCount}] Running[${runningCount}] Pending[${pendingCount}] ElapsedTime[${totalSeconds.toFixed(3)}s] 🚀`.green);
  console.log(`Rollout state: ${rolloutState} 🚀`.green);
  process.exit(0);
})();
